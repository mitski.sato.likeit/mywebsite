package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/Master_BookDataDetail")
public class Master_BookDataDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Master_BookDataDetail() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションのユーザー情報が管理者でなければ、ログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//漫画一覧（管理者用）から選択した商品のIDを取得
		String id = request.getParameter("id");

		//取得したIDを元に商品の情報を取得
		ItemDao itemdao = new ItemDao();
		ItemDataBeans book = itemdao.findbook(id);

		//商品の情報をリクエストスコープにセット
		request.setAttribute("book", book);

		//商品マスター情報詳細jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookDataDetail.jsp");
		dispatcher.forward(request, response);

	}
}
