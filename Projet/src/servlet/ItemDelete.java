package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;


@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ItemDelete() {
        super();

    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//商品の削除ボタンからIDを取得
		String id = request.getParameter("id");

		//セッションからカート情報を取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//選択した商品のIDを元にカートから商品を削除
		if(id != null) {
			for(ItemDataBeans cartInItem : cart) {
				if(cartInItem.getId() ==  Integer.parseInt(id)) {
					cart.remove(cartInItem);
					break;
				}
			}
		}

		//セッションにカート情報をセット
		session.setAttribute("cart", cart);
		//カートサーブレットに移行
		response.sendRedirect("Cart");
	}
}

