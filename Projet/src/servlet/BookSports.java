package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/BookSports")
public class BookSports extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public BookSports() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//ジャンルをスポーツに設定して、全スポーツ漫画の情報を取得
		String sports = "スポーツ";
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> bookList = itemdao.findJanru(sports);

		//リクエストスコープにスポーツ漫画の情報をセット
		request.setAttribute("bookList", bookList);

		//スポーツ漫画jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BookSports.jsp");
		dispatcher.forward(request, response);
	}
}
