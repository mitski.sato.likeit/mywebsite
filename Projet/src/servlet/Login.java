package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Login() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログインjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
		dispatcher.forward(request, response);

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		//入力されたログインIDとパスワードを取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		//入力されたログインID、パスワードからユーザーを探す
		UserDao userdao = new UserDao();
		UserDataBeans user = userdao.findUser(loginId, password);


		//テーブルにユーザ情報がなければ、エラーメッセージを表示(ログイン失敗時)
		if (user == null) {
		request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");

		// ログインjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
		dispatcher.forward(request, response);
		return;
		}

		// セッションにユーザー情報とユーザーIDをセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
		session.setAttribute("userId", user.getId());

		//インデックスjspに移行
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Index.jsp");
		dispatcher.forward(request, response);
	}
}
