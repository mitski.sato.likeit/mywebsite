package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

@WebServlet("/UserDataDetail")
public class UserDataDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserDataDetail() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログインjspにフォワード
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションに入っているユーザIDを取得
		int userId =  (int) session.getAttribute("userId");

		//ユーザーIDを元にユーザー情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans User = userDao.findParameter(userId);

		//リクエストスコープにユーザー情報をセット
		request.setAttribute("user", User);

		//ユーザ詳細jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDataDetail.jsp");
		dispatcher.forward(request, response);

	}
}
