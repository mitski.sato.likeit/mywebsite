package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/Master_BookList")
public class Master_BookList extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public Master_BookList() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションのユーザー情報が管理者でなければ、ログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//商品の情報を全て取得
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> bookList = itemdao.findAll();

		//リクエストスコープに商品情報をセット
		request.setAttribute("bookList", bookList);

		//漫画一覧（管理者用）jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookList.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		//入力された作品名を取得
		String name = request.getParameter("name");

		//取得した作品名で検索
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> bookList = itemdao.searchBook(name);

		//検索でヒットした作品をリクエストスコープにセット
		request.setAttribute("bookList", bookList);

		//漫画一覧（管理者用）jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookList.jsp");
		dispatcher.forward(request, response);
	}

}
