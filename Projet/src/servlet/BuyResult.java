package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;


@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public BuyResult() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		//セッション
		HttpSession session = request.getSession();
		//セッションからカート情報を取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//セッションから購入情報を取得し、データベースに登録
		BuyDataBeans bdb = (BuyDataBeans) session.getAttribute("BDB");
		BuyDao.insertBuy(bdb);

		//ユーザIDから最新の購入IDを探す
		int num = bdb.getUserId();
		BuyDao buydao = new BuyDao();
		BuyDataBeans Num = buydao.findBuyId(num);
		int id = Num.getId();

		//購入した商品と共にデータベースに登録
		for (ItemDataBeans item : cart) {
			BuyDetailDataBeans bddb= new BuyDetailDataBeans();
			bddb.setBuyId(id);
			bddb.setItemId(item.getId());
			BuyDetailDao.insertBuyDetail(bddb);
		}

		//カート情報をセッションから削除
		session.removeAttribute("cart");

		//購入情報をリクエストスコープにセット
		BuyDataBeans resultBDB = BuyDao.getBuyDataBeansByBuyId(id);
		request.setAttribute("resultBDB", resultBDB);

		// 購入アイテム情報をリクエストスコープにセット
		List<ItemDataBeans> buyIDBList = BuyDetailDao.getItemDataBeansListByBuyId(id);
		request.setAttribute("buyIDBList", buyIDBList);

		//購入完了jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BuyResult.jsp");
		dispatcher.forward(request, response);
	}

}
