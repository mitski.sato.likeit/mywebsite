package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;


@WebServlet("/Logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Logout() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションからユーザー情報、購入情報、カート情報を削除
		session.removeAttribute("userId");
		session.removeAttribute("userInfo");
		session.removeAttribute("BDB");
		session.removeAttribute("cart");

		//ログアウトjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Logout.jsp");
		dispatcher.forward(request, response);

	}
}
