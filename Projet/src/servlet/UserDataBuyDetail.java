package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.BuyDetailDao;
import dao.ItemDao;


@WebServlet("/UserDataBuyDetail")
public class UserDataBuyDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public UserDataBuyDetail() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面にフォワード
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションに入っているユーザIDを取得
		int userId =  (int) session.getAttribute("userId");

		//購入履歴画面の詳細ボタンから送られてくるIDを参照
		String buyId = request.getParameter("id");

		//ユーザーIDと購入IDを元に当時の購入履歴の詳細を表示
		BuyDetailDao bdddao = new BuyDetailDao();
		BuyDataBeans buy = bdddao.findBuyDataByUserIdAndBuyId(userId, buyId);

		//購入履歴詳細情報をリクエストスコープにセット
		request.setAttribute("buy", buy );

		//ユーザーIDと購入IDを元に当時購入した商品を取得
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> itemList = itemdao.findItemByUserIdAndBuyId(userId, buyId);

		//取得した商品情報をリクエストスコープにセット
		request.setAttribute("itemList", itemList);

		//購入履歴詳細jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDataBuyDetail.jsp");
		dispatcher.forward(request, response);

	}
}
