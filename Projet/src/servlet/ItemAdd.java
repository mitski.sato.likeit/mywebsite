package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/ItemAdd")
public class ItemAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ItemAdd() {
        super();

    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//商品詳細画面から商品のIDを取得
		String id = request.getParameter("id");

		//商品IDから商品の情報を取得
		ItemDao itemdao = new ItemDao();
		ItemDataBeans book= itemdao.findbook(id);

		//リクエストスコープに商品情報をセット
		request.setAttribute("book", book);

		//セッションからカート情報を取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//カートに商品がなければ、カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemDataBeans>();
			session.setAttribute("cart", cart);
		}

		//カートに選択した商品を入れる
		cart.add(book);

		//セッションにカート情報をセット
		session.setAttribute("cart", cart);
		//カートサーブレットにリダイレクト
		response.sendRedirect("Cart");
	}

}
