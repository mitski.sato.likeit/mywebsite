package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

@WebServlet("/UserDataUpdate")
public class UserDataUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserDataUpdate() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザ情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログインjspに移行する
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		// URLからユーザIDを受け取る
		String id = request.getParameter("id");

		// ユーザーIDを元にユーザー情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans User = userDao.findParameter(id);

		//リクエストスコープにユーザー情報をセット
		request.setAttribute("user", User);

		//ユーザー情報更新jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDataUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// 更新画面で入力された情報を取得
		String login_id = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birthDate");

		//入力欄に空白があった場合//
		if (password.equals("") || password2.equals("") || name.equals("") || birth_date.equals("")) {

			// エラーメッセージ
			request.setAttribute("errMsg", "入力されていない項目が存在しました。");

			// 管理者ユーザー情報更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDataUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードが一致しない
		if (!password.equals(password2)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが一致しませんでした。");

			//管理者ユーザー情報更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDataUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ユーザー情報を更新
		UserDao userdao = new UserDao();
		userdao.Update(password, name, birth_date, login_id);

		//更新したユーザ情報をユーザ詳細画面で表示
		UserDao userdao2 = new UserDao();
		UserDataBeans user = userdao2.findByloginIdParameter(login_id);

		request.setAttribute("user", user);

		//ユーザ詳細jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDataDetail.jsp");
		dispatcher.forward(request, response);

	}
}
