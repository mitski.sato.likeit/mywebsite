package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;


@WebServlet("/Admin_UserList")
public class Admin_UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Admin_UserList() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションのユーザー情報が管理者でなければログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//データベースに入っている全ユーザー情報を取得（管理者以外）
		UserDao userDao = new UserDao();
		List<UserDataBeans> userList = userDao.findAll();

		//リクエストスコープに全ユーザー情報をセット
		request.setAttribute("userList", userList);

		//管理者ユーザ一覧jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Admin_UserList.jsp");
		dispatcher.forward(request, response);

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//入力されたログインID、名前、日時の範囲を取得
		String login_id = request.getParameter("login_id");
		String name = request.getParameter("name");
		String dateStart = request.getParameter("date_start");
		String dateEnd = request.getParameter("date_end");

		//取得した条件に当てはまるユーザーを検索
		UserDao userDao = new UserDao();
		List<UserDataBeans> userList = userDao.searchUser(login_id, name, dateStart, dateEnd);

		//リクエストスコープにユーザー情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Admin_UserList.jsp");
		dispatcher.forward(request, response);

	}

}
