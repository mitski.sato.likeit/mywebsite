package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;


@WebServlet("/Admin_UserDataDetail")
public class Admin_UserDataDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Admin_UserDataDetail() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();

		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションに残っているユーザー情報が管理者でなければログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}

		//管理者ユーザー一覧画面、詳細ボタンからユーザーIDを取得
		String id = request.getParameter("id");

		//取得したユーザーIDからユーザー情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans userList = userDao.findParameter(id);

		//リクエストスコープにユーザー情報をセット
		request.setAttribute("userList", userList);

		//管理者ユーザー情報詳細jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Admin_UserDataDetail.jsp");
		dispatcher.forward(request, response);
	}
}
