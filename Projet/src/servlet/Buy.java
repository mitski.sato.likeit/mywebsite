package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.DeliveryDao;


@WebServlet("/Buy")
public class Buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Buy() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//選択した配送方法のIDを取得
		String id = request.getParameter("delivery_id");
		//セッションからユーザーIDを取得
		int userId =  (int) session.getAttribute("userId");

		//配達IDから配達情報を取得
		DeliveryDao deliverydao = new DeliveryDao();
		DeliveryDataBeans ddb =  deliverydao.findDelivery(id);

		//リクエストスコープにセット
		request.setAttribute("ddb", ddb);

		//セッションからカート情報を取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//カートの中の商品の値段の合計を計算
	    int totalprice = 0;
		for(ItemDataBeans cartPrice : cart) {
			totalprice += cartPrice.getPrice();
		}
		totalprice += ddb.getPrice();

		//購入情報を変数に代入
		BuyDataBeans BDB = new BuyDataBeans();
		BDB.setTotalPrice(totalprice);
		BDB.setUserId(userId);
		BDB.setDeliveryId(ddb.getId());

		//購入情報をリクエストスコープにセット
		request.setAttribute("BDB", BDB);
		//購入情報をセッションにセット
		session.setAttribute("BDB", BDB);

		//購入確定画面jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Buy.jsp");
		dispatcher.forward(request, response);
	}
}
