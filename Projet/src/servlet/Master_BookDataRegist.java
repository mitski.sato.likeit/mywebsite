package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;

@WebServlet("/Master_BookDataRegist")
public class Master_BookDataRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Master_BookDataRegist() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションのユーザー情報が管理者でなければ、ログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//商品マスター漫画新規登録jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookDataRegist.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//作品新規登録画面で入力された作品名、ジャンル、金額を取得
		String name = request.getParameter("name");
		String janru = request.getParameter("janru");
		String price = request.getParameter("price");
		ItemDao itemdao = new ItemDao();

		//入力欄に空白があった場合//
		if (name.equals("") || janru.equals("") || price.equals("")) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力されていない項目が存在します。");

			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookDataRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//入力された作品が既に存在する場合
		ItemDataBeans Name = itemdao.findByName(name);
		if (Name != null) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "既に登録されている作品です。");

			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookDataRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//作品を新規登録
		itemdao.RegisterBook(name, janru, price);

		//商品マスタ一覧サーブレットにリダイレクト
		response.sendRedirect("Master_BookList");

	}
}
