package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/BookLove")
public class BookLove extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public BookLove() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//ジャンルを恋愛に設定して、全恋愛漫画を表示
		String love = "恋愛";
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> bookList = itemdao.findJanru(love);

		//恋愛漫画の情報をリクエストスコープにセット
		request.setAttribute("bookList", bookList);

		//恋愛漫画jpsにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BookLove.jsp");
		dispatcher.forward(request, response);
	}
}
