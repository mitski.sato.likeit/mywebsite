package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;

@WebServlet("/Master_BookDataDelete")
public class Master_BookDataDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Master_BookDataDelete() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションのユーザー情報が管理者でなければ、ログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//漫画一覧で選択した商品のIDを取得
		String id = request.getParameter("id");

		//商品のIDから商品の情報を取得
		ItemDao itemdao = new ItemDao();
		ItemDataBeans book = itemdao.findbook(id);

		//リクエストスコープに商品の情報をセット
		request.setAttribute("book", book);

		//商品情報削除jspに移行
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookDataDelete.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//選択した商品のIDを取得
		String id = request.getParameter("id");

		//商品のIDを元に商品を削除
		ItemDao itemdao = new ItemDao();
		itemdao.DeleteBook(id);

		//漫画一覧（管理者用）サーブレットにリダイレクト
		response.sendRedirect("Master_BookList");

	}
}
