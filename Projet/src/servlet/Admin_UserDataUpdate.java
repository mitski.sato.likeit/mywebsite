package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;


@WebServlet("/Admin_UserDataUpdate")
public class Admin_UserDataUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin_UserDataUpdate() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();

		//セッションからユーザー情報を取得
		UserDataBeans User = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (User == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションに残っているユーザー情報が管理者でなければログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//管理者ユーザー一覧画面、更新ボタンからユーザーIDを取得
		String id = request.getParameter("id");

		//取得したユーザーIDからユーザー情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findParameter(id);

		//リクエストスコープにユーザー情報をセット
		request.setAttribute("user", user);

		//管理者ユーザー情報更新jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Admin_UserDataUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//jspで入力された情報を取得
		String login_id = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birthDate");

		//入力欄に空白があった場合//
		if (password.equals("") || password2.equals("") || name.equals("") || birth_date.equals("")) {

			// エラーメッセージ
			request.setAttribute("errMsg", "入力されていない項目が存在しました。");

			// 管理者ユーザー情報更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Admin_UserDataUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードが一致しない
		if (!password.equals(password2)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが一致しませんでした。");

			//管理者ユーザー情報更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Admin_UserDataUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ユーザー情報を更新
		UserDao userdao = new UserDao();
		userdao.Update(password, name, birth_date, login_id);

		// ユーザ一覧にリダイレクト
		response.sendRedirect("Admin_UserList");
	}
}
