package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDetailDao;


@WebServlet("/UserDataBuy")
public class UserDataBuy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserDataBuy() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションからユーザーIDを取得
		int userId =  (int) session.getAttribute("userId");

		//ユーザーIDからユーザーの購入履歴を取得
		BuyDetailDao bdddao = new BuyDetailDao();
		List<BuyDataBeans> buyList = bdddao.findBuyDataByUserId(userId);

		//購入履歴をリクエストスコープにセット
		request.setAttribute("buyList", buyList);

		//ユーザー購入履歴jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDataBuy.jsp");
		dispatcher.forward(request, response);

	}
}
