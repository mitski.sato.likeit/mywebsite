package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class Admin_UserDataDelete
 */
@WebServlet("/Admin_UserDataDelete")
public class Admin_UserDataDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public Admin_UserDataDelete() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans User = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (User == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションに残っているユーザ情報が管理者でなければログインに画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//ユーザー一覧画面の削除ボタンからユーザーIDを取得
		String id = request.getParameter("id");

		//取得したユーザーIDからユーザー情報を検索
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findParameter(id);

		//リクエストスコープにユーザー情報をセット
		request.setAttribute("user", user);

		//管理者ユーザー情報削除jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Admin_UserDataDelete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//ユーザーのログインIDを取得
		String loginId = request.getParameter("loginId");

		//ユーザーのログインIDを元にユーザー情報を削除
		UserDao userDao = new UserDao();
		userDao.Delete(loginId);

		//管理者ユーザー一覧ページにリダイレクト
		response.sendRedirect("Admin_UserList");
	}
}
