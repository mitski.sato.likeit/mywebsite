package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/BookOther")
public class BookOther extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public BookOther() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//ジャンルをその他に設定して、全その他の漫画の情報を取得
		String other = "その他";
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> bookList = itemdao.findJanru(other);

		//リクエストスコープにその他の漫画情報をセット
		request.setAttribute("bookList", bookList);

		//その他の漫画jspに移行
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BookOther.jsp");
		dispatcher.forward(request, response);
	}
}
