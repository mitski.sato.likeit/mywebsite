package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.DeliveryDao;


@WebServlet("/BuySelect")
public class BuySelect extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public BuySelect() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションからカート情報を取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//配達情報を取得して、リクエストスコープにセット
		DeliveryDao deliverydao = new DeliveryDao();
		List<DeliveryDataBeans> ddbList = deliverydao.SelectDeliveryMethod();
		request.setAttribute("ddbList", ddbList);

		//購入選択画面jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BuySelect.jsp");
		dispatcher.forward(request, response);

	}
}
