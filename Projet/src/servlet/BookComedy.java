package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/BookComedy")
public class BookComedy extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public BookComedy() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//ジャンルをギャグに設定し、全ギャグ漫画を検索
		String comedy = "ギャグ";
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> bookList = itemdao.findJanru(comedy);

		//リクエストスコープに漫画の情報をセット
		request.setAttribute("bookList", bookList);

		//ギャグ漫画jspに移行
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BookComedy.jsp");
		dispatcher.forward(request, response);
	}
}
