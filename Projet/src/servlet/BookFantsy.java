package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/BookFantsy")
public class BookFantsy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public BookFantsy() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//ジャンルをSF・ファンタジーに設定し、全SF・ファンタジー漫画を取得
		String fantsy = "SF・ファンタジー";
		ItemDao itemdao = new ItemDao();
		List<ItemDataBeans> bookList = itemdao.findJanru(fantsy);

		//リクエストスコープにSF・ファンタジー漫画の情報をセット
		request.setAttribute("bookList", bookList);

		//SF・ファンタジー漫画jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BookFantsy.jsp");
		dispatcher.forward(request, response);
	}
}
