package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;


@WebServlet("/Master_BookDataUpdate")
public class Master_BookDataUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Master_BookDataUpdate() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();
		//セッションからユーザー情報を取得
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");

		//セッションにユーザー情報がなければ、ログイン画面に移行
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//セッションのユーザー情報が管理者でなければ、ログイン画面に移行
		int userId = (int) session.getAttribute("userId");
		if (userId != 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

		//選択された商品のIDを取得
		String id = request.getParameter("id");

		//取得したIDを元に商品の情報を取得
		ItemDao itemdao = new ItemDao();
		ItemDataBeans book = itemdao.findbook(id);

		//リクエストスコープに商品の情報をセット
		request.setAttribute("book", book);

		//商品マスター情報更新画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookDataUpdate.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//更新画面で入力された、作品名、ジャンル、単価
		//選択した商品のIDを取得
		String name = request.getParameter("name");
		String janru = request.getParameter("janru");
		String price = request.getParameter("price");
		String id = request.getParameter("id");
		ItemDao itemdao = new ItemDao();

		//入力欄に空白がある（エラー）
		 if ( name.equals("") || janru.equals("") || price.equals("")) {
			 //リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力されていない項目が存在します。");

			//商品マスタ情報更新画面jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Master_BookDataUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		 //商品情報を更新
		itemdao.UpdateBook(name, janru, price, id);

		// 商品マスタ一覧にリダイレクト
		response.sendRedirect("Master_BookList");
	}
}
