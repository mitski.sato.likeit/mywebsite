package beans;

import java.io.Serializable;

public class ItemDataBeans implements Serializable{

	private int id;
	private String name;
	private int price;
	private String janru;

	public ItemDataBeans() {

	}


	public ItemDataBeans(int id, String name, int price, String janru) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.janru = janru;
	}

	public ItemDataBeans(String name, int price, String janru) {
		super();
		this.name = name;
		this.price = price;
		this.janru = janru;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getJanru() {
		return janru;
	}
	public void setJanru(String janru) {
		this.janru = janru;
	}

}
