package beans;

import java.io.Serializable;
import java.sql.Date;

public class BuyDataBeans implements Serializable{

	private int id;
	private Date buyDate;
	private int totalPrice;
	private int userId;
	private int deliveryId;

	private int itemprice;
	private String itemname;
	private String deliveryname;


	public BuyDataBeans(int id, Date buyDate, int totalPrice, String deliveryname) {
		super();
		this.id = id;
		this.buyDate = buyDate;
		this.totalPrice = totalPrice;
		this.deliveryname = deliveryname;
	}

	public BuyDataBeans(int id, Date buyDate, int totalPrice, int userId, int deliveryId, String itemname, String deliveryname) {
		super();
		this.id = id;
		this.buyDate = buyDate;
		this.totalPrice = totalPrice;
		this.userId = userId;
		this.deliveryId = deliveryId;
		this.itemname = itemname;
		this.deliveryname = deliveryname;

	}



	public BuyDataBeans(Date buyDate, int totalPrice,  String itemname, int itemprice, String deliveryname) {
		super();
		this.buyDate = buyDate;
		this.totalPrice = totalPrice;
		this.itemname = itemname;
		this.itemprice = itemprice;
		this.deliveryname = deliveryname;
	}

	public BuyDataBeans(int id) {
		this.id = id;
	}

	public BuyDataBeans(int id, Date buyDate, int totalPrice, String itemName, int itemPrice, String deliveryName) {
		super();
		this.id = id;
		this.buyDate = buyDate;
		this.totalPrice = totalPrice;
		this.itemname = itemName;
		this.itemprice = itemPrice;
		this.deliveryname = deliveryName;
	}

	public BuyDataBeans() {
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(int deliveryId) {
		this.deliveryId = deliveryId;
	}
	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getDeliveryname() {
		return deliveryname;
	}

	public void setDeliveryname(String deliveryname) {
		this.deliveryname = deliveryname;
	}

	public int getItemprice() {
		return itemprice;
	}

	public void setItemprice(int itemprice) {
		this.itemprice = itemprice;
	}


}
