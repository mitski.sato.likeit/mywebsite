package beans;

import java.io.Serializable;
import java.sql.Date;

public class UserDataBeans implements Serializable {

	private int id;
	private String loginId;
	private String password;
	private String name;
	private Date birthDate;
	private Date createDate;
	private Date updateDate;

	public UserDataBeans() {

	}


	public UserDataBeans(int id, String loginId, String password, String name, Date birthDate, Date createDate,
			Date upDate) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = upDate;
	}

	public UserDataBeans(String loginId, String name, Date birthDate, Date createDate, Date upDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = upDate;
	}


	public UserDataBeans(int id, String loginId, String name) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
	}

	public UserDataBeans(String loginId) {
		this.loginId = loginId;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpDate() {
		return updateDate;
	}
	public void setUpDate(Date upDate) {
		this.updateDate = upDate;
	}
}
