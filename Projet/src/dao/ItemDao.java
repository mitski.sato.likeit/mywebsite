package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDao {

	//商品マスタ一覧で全ての漫画を表示する
	public List<ItemDataBeans> findAll() {

		Connection conn = null;
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				String janru = rs.getString("janru");
				ItemDataBeans item = new ItemDataBeans(id, name, price, janru);

				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//漫画一覧（管理者用）で作品を名前検索する
	public List<ItemDataBeans> searchBook(String name) {

		String sql = "SELECT * FROM item WHERE name LIKE '%" + name + "%'";

		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			Statement pStmt = conn.createStatement();
			ResultSet rs = pStmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String Mname = rs.getString("name");
				int price = rs.getInt("price");
				String janru = rs.getString("janru");
				ItemDataBeans item = new ItemDataBeans(id, Mname, price, janru);

				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//商品マスタ詳細画面でマンガの詳細を表示
	public ItemDataBeans findbook(String id) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id = rs.getInt("id");
			String name = rs.getString("name");
			int price = rs.getInt("price");
			String janru = rs.getString("janru");
			return new ItemDataBeans(Id, name, price, janru);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//商品マスタ情報更新画面で作品情報を更新
	public void UpdateBook(String name, String janru, String price, String id) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "UPDATE item SET name = ?,janru = ?,price = ? WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, janru);
			pStmt.setString(3, price);
			pStmt.setString(4, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//商品マスタ一覧画面で選択した商品を削除します
	public void DeleteBook(String id) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM item WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//商品マスタ画面で新しい漫画を新規登録する
	public void RegisterBook(String name, String janru, String price) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO item (name, janru, price) VALUE (?, ?, ?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, janru);
			pStmt.setString(3, price);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//作品一覧画面のそれぞれのジャンルボタンからジャンルの作品を表示
	public List<ItemDataBeans> findJanru(String janru) {

		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item WHERE janru=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, janru);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String Mname = rs.getString("name");
				int price = rs.getInt("price");
				String Janru = rs.getString("janru");
				ItemDataBeans item = new ItemDataBeans(id, Mname, price, Janru);

				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
		return itemList;
	}

	//商品の名前から作品を探す（新規登録の際、同じ商品が存在するか？）
	public ItemDataBeans findByName(String name) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT *FROM item WHERE name = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id = rs.getInt("id");
			String Name = rs.getString("name");
			int price = rs.getInt("price");
			String janru = rs.getString("janru");
			return new ItemDataBeans(Id, Name, price, janru);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//購入履歴詳細画面で購入した商品名と単価を表示
	public List<ItemDataBeans> findItemByUserIdAndBuyId(int userId, String buyId) {

		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT i.name AS itemname, i.price AS itemprice, i.janru"
						+" FROM  buy_detail b1 "
						+" INNER JOIN buy b "
						+" ON b1.buy_id = b.id "
						+" INNER JOIN item i "
						+" ON b1.item_id = i.id "
						+" WHERE b.user_id = ? AND b.id = ? "
						+" ORDER BY b.id DESC ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1,userId);
			pStmt.setString(2, buyId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				String Mname = rs.getString("itemname");
				int price = rs.getInt("itemprice");
				String Janru = rs.getString("janru");
				ItemDataBeans item = new ItemDataBeans(Mname, price, Janru);

				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}	return itemList;
	}
}