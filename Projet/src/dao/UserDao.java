package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class UserDao {
	//ログイン画面で入力されたログインID、パスワードからユーザを探す
	public UserDataBeans findUser(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//ユーザ特定のsql文
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			//SELECTを実行し、結果を取得//
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理//
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理//
			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new UserDataBeans(idData, loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//新規登録画面で入力された情報でユーザを登録する。
	public void Register(String loginId, String password, String name, String birth_date) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user(login_id, password, name, birthday_date, create_date, update_date)"
					+ "VALUES(?,?,?,?,now(),now())";

			//パスワードの暗号化
			String password2 = encryption(password);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password2);
			pStmt.setString(3, name);
			pStmt.setString(4, birth_date);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//タイトル画面のユーザ詳細ボタンからidを受け取り情報を表示
	public UserDataBeans findParameter(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT login_id, name, birthday_date, create_date, update_date FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birthday_date");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			return new UserDataBeans(loginId, name, birthDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//セッションから受け取ったIDからユーザを検索
	public UserDataBeans findParameter(int id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT login_id, name, birthday_date, create_date, update_date FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birthday_date");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			return new UserDataBeans(loginId, name, birthDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザ情報更新
	public void Update(String password, String name, String birth_date, String login_id) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET password= ?,name= ?,birthday_date= ?,update_date=now() WHERE login_id= ?";

			//パスワードの暗号化
			String password2 = encryption(password);

			//UPDATE文を実行//
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, password2);
			pStmt.setString(2, name);
			pStmt.setString(3, birth_date);
			pStmt.setString(4, login_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザ情報を更新した後、ユーザ詳細画面で更新した情報を表示
	public UserDataBeans findByloginIdParameter(String login_id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT login_id, name, birthday_date, create_date, update_date FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birthday_date");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			return new UserDataBeans(loginId, name, birthDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザ一覧で全てのユーザを表示させる
	public List<UserDataBeans> findAll() {
		Connection conn = null;
		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文の準備//
			String sql = "SELECT * FROM user WHERE id != 1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birthday_date");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("update_date");
				UserDataBeans user = new UserDataBeans(id, loginId, password, name, birthDate, createDate, upDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//ユーザ一覧画面でユーザを検索する
	public List<UserDataBeans> searchUser(String login_id, String name, String dateStart, String dateEnd) {

		String sql = "SELECT * FROM user WHERE login_id != 'admin' ";

		//ログインIDの条件
		if (!login_id.equals("")) {
			sql += " and login_id = '" + login_id + "' ";
		}

		//名前の条件
		if (!name.equals("")) {
			sql += " and name LIKE '%" + name + "%' ";
		}

		//生年月日の条件
		if (!dateStart.equals("") || !dateEnd.equals("")) {
			sql += " and birthday_date >= '" + dateStart + "' and birthday_date < '" + dateEnd + "'";
		}

		List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//SELECTを実行し、結果を取得//
			Statement pStmt = conn.createStatement();
			ResultSet rs = pStmt.executeQuery(sql);

			//検索成功時の処理//
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String Uname = rs.getString("name");
				Date birthDate = rs.getDate("birthday_date");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("update_date");
				UserDataBeans user = new UserDataBeans(id, loginId, password, Uname, birthDate, createDate, upDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//ユーザ削除画面でユーザ情報削除
	public void Delete(String loginId) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//新規登録時に同じログインIDを使用していないか判別（ログインIDからユーザを探す）
	public UserDataBeans findByloginId(String login_id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//ユーザ特定のsql文
			String sql = "SELECT * FROM user WHERE login_id = ? ";

			//SELECTを実行し、結果を取得//
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理//
			if (!rs.next()) {
				return null;
			}

			String loginId = rs.getString("login_id");
			return new UserDataBeans(loginId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//パスワードの暗号化
	public String encryption(String password) {

		try {
			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;

			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力

			System.out.println(result);
			return result;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
}