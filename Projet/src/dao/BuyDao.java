package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.BuyDataBeans;

public class BuyDao {

	//購入した商品をデータベースに登録する
	public static void insertBuy(BuyDataBeans bdb) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO buy (buy_date, total_price, user_id ,delivery_id) VALUES(NOW(),?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, bdb.getTotalPrice());
			pStmt.setInt(2, bdb.getUserId());
			pStmt.setInt(3, bdb.getDeliveryId());
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	// 購入した時のIDを探す
	public BuyDataBeans findBuyId(int num) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT MAX(id) FROM buy WHERE user_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, num);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			int id = rs.getInt("MAX(id)");
			return new BuyDataBeans(id);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//購入情報を表示
	public static BuyDataBeans getBuyDataBeansByBuyId(int id) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT b.buy_date,"
					+ "b.total_price,"
					+ "i.name AS itemname,"
					+ "i.price,"
					+ "d.name AS deliveryname"
					+ " FROM buy_detail b1 "
					+ " INNER JOIN buy b "
					+ " ON b1.buy_id = b.id "
					+ " INNER JOIN item i "
					+ " ON b1.item_id = i.id "
					+ " INNER JOIN delivery d "
					+ " ON d.id = b.delivery_id "
					+ " WHERE b.id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			Date buyDate = rs.getDate("buy_date");
			int totalPrice = rs.getInt("total_price");
			String itemName = rs.getString("itemname");
			int itemPrice= rs.getInt("price");
			String deliveryName = rs.getString("deliveryname");

			return new BuyDataBeans(buyDate, totalPrice, itemName, itemPrice, deliveryName);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}