package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.DeliveryDataBeans;

public class DeliveryDao {

	//購入選択画面で配達方法を選択する
	public List<DeliveryDataBeans> SelectDeliveryMethod() {

		List<DeliveryDataBeans> ddbList = new ArrayList<DeliveryDataBeans>();

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM delivery" ;

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				DeliveryDataBeans ddb = new DeliveryDataBeans(id, name, price);

				ddbList.add(ddb);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return ddbList;
	}

	//購入確定画面で選択した配達方法をidを元に探し、表示する
	public DeliveryDataBeans findDelivery(String id) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

		String sql = "SELECT * FROM delivery WHERE id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		int Id = rs.getInt("id");
		String name = rs.getString("name");
		int price = rs.getInt("price");
		return new DeliveryDataBeans(Id, name, price);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
