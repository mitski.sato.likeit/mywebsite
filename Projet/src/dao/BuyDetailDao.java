package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

public class BuyDetailDao {

	//購入した商品の詳細をデータベースBuyDetailテーブルに登録する
	public static void insertBuyDetail(BuyDetailDataBeans bddb) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO buy_detail (buy_id, item_id) VALUES (?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, bddb.getBuyId());
			pStmt.setInt(2, bddb.getItemId());
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}

	//購入した商品の詳細
	public static List<ItemDataBeans> getItemDataBeansListByBuyId(int id) {

		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT item.* "
					+ " FROM item "
					+ " INNER JOIN buy_detail "
					+ " ON item.id = buy_detail.item_id "
					+ " WHERE buy_detail.buy_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int Id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				String janru = rs.getString("janru");
				ItemDataBeans item = new ItemDataBeans(Id, name, price, janru);

				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//ユーザ購入履歴画面で履歴を表示する
	public List<BuyDataBeans> findBuyDataByUserId(int userId) {

		List<BuyDataBeans> buyList = new ArrayList<BuyDataBeans>();

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT b.id, b.buy_date, b.total_price, d.name AS deliveryname "
					+ " FROM buy b "
					+ " INNER JOIN delivery d "
					+ " ON d.id = b.delivery_id "
					+ " WHERE b.user_id = ? "
					+ " ORDER BY b.id DESC ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				Date buyDate = rs.getDate("buy_date");
				int totalPrice = rs.getInt("total_price");
				String deliveryName = rs.getString("deliveryname");
				BuyDataBeans buy = new BuyDataBeans(id, buyDate, totalPrice, deliveryName);

				buyList.add(buy);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return buyList;
	}

	//購入履歴詳細画面で購入日時と配達方法と合計金額を表示
	public BuyDataBeans findBuyDataByUserIdAndBuyId(int userId, String buyId) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT b.id, b.buy_date, b.total_price,d.name AS deliveryname "
					+ " FROM   buy b "
					+ " INNER JOIN delivery d "
					+ " ON d.id = b.delivery_id "
					+ " WHERE b.user_id = ? AND b.id = ? "
					+ " ORDER BY b.id DESC ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.setString(2, buyId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			Date buyDate = rs.getDate("buy_date");
			int totalPrice = rs.getInt("total_price");
			String deliveryname = rs.getString("deliveryname");
			return new BuyDataBeans(id, buyDate, totalPrice, deliveryname);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
