<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>カート画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　      <h1>マンガフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>カート</h2><br>
			${cartActionMessage}
        </div>
        <div style="text-align:center;">
    		<c:if test="${ErrorMessage != null}" >
				<span style="color:red;">${ErrorMessage}</span>
			</c:if>
		</div>
             <div class="jumbotron jumbotron-fluid">
                 <div class="container">
                     <table align="center" width="600">
                         <tr>
                             <td bgcolor="#00FFFF" align="center">作品名</td>
                             <td bgcolor="#8FBC8F" align="center">ジャンル</td>
                             <td bgcolor="#FFA500" align="center">価格</td>
                             <td></td>
                         </tr>
                         <c:forEach var="book" items="${cart}">
                         <tr>
                             <td height="40" align="center">${book.name}</td>
                             <td align="center">${book.janru}</td>
                             <td align="center">${book.price}円</td>
                             <td align="center"><a href="ItemDelete?id=${book.id}" class="btn btn-danger">削除</a></td>
                         </tr>
                         </c:forEach>
                     </table>
                     <p style="margin:20px;"></p>
                     <div style="text-align:center;">
	                     <c:if test="${cart.size() != 0}" >
	                         <a href="BuySelect" class="btn btn-primary">購入選択画面へ</a>
	                     </c:if>
                     </div>
                 </div>
             </div>
             <div style="text-align:center;">
                 <p style="margin:10px;"></p>
                 <a href="BookList" class="btn btn-success">買い物を続ける</a>
                 <p style="margin:40px;"></p>
                 <p style="margin-bottom:20px;">
                 <a href="Index" class="btn-flat-simple">
                 <i class="fa fa-caret-right" ></i><u>TOPへ</u></a></p>
             </div>
	</body>
</html>
