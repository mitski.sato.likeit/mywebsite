<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細（管理者用）</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
            <h1>マンガフェスタ</h1>
        </div>
    </div>
</head>
	<body>
         <div style="text-align:center;">
            <h2>ユーザ情報</h2>
        </div>
	       <form action="" method="get">
           <p style="margin:20px;"></p>
               <table align="center" width="300">
                    <tr>
                        <td height="40" align="center">ログインID</td>
                        <td align="center">${userList.loginId}</td>
                    </tr>
                    <tr>
                        <td height="40" align="center">ユーザ名</td>
                        <td align="center">${userList.name }</td>
                    </tr>
                    <tr>
                        <td height="40" align="center">生年月日</td>
                        <td align="center">${userList.birthDate }</td>
                    </tr>
                    <tr>
                        <td align="center" height="40">登録日時</td>
                        <td align="center">${userList.createDate}</td>
                    </tr>
                    <tr>
                        <td align="center" height="40">更新日時</td>
                        <td align="center">${userList.upDate}</td>
                    </tr>
                </table>
                <p style="margin:$0px;"></p>
            </form>
        <div style="text-align:center;">
            <a class="btn btn-outline-secondary"href="Admin_UserList">戻る</a>
        </div>
	</body>
</html>
