<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>漫画一覧（管理者用）</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
	      <h1>マンガフェスタ</h1>
    	</div>
	       <div style="text-align:center;">
    	      <a href="Index" class="navbar-link logout-link"><u>タイトルに戻る</u></a>
		</div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
             <p><h3>漫画リスト（管理者用）</h3>
             <p style="margin:20px;"></p>
	      <a class="btn btn-primary btn-lg" href="Master_BookDataRegist">漫画を新規登録</a>
        </div>
        <p style="margin:40px;"></p>
        <div style="text-align:center;">
        <form action="Master_BookList" method="post">
            <input type="submit" value="検索">&nbsp;&nbsp;
            <input type="text" placeholder="作品名" name="name" size="40">
        </form>
        </div>
        <p style="margin:20px;"></p>
        <div style="text-align:center;">
            <h4>現在の登録リスト</h4>
        </div>
        <table  border="3" align="center" >
            <tr>
                <td bgcolor="#00FFFF" height="60" align="center">作品名</td>
                <td bgcolor="#8FBC8F" align="center">ジャンル</td>
                <td bgcolor="#FFA500" align="center">単価</td>
                <td bgcolor="#E6E6FA" align="center">詳細&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;更新&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;削除</td>
            </tr>
            <c:forEach var="bookList" items="${bookList}">
            <tr>
                <td align="center">${bookList.name}</td>
                <td align="center">${bookList.janru}</td>
                <td height="40" align="center">${bookList.price}円</td>
                <td height="40" align="center"><a class="btn btn-primary" href="Master_BookDataDetail?id=${bookList.id}">詳細</a>
                <a class="btn btn-warning" href="Master_BookDataUpdate?id=${bookList.id}">更新</a>
                <a class="btn btn-danger" href="Master_BookDataDelete?id=${bookList.id}">削除</a></td>
            </tr>
            </c:forEach>
        </table>
    </body>
</html>
