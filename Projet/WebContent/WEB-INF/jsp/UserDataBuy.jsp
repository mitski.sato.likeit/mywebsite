<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ購入履歴画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　      <h1>マンガフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
        <h2>ユーザ購入履歴</h2>
        </div>
            <table border="3" align="center">
                <tr>
                    <td style="background-color:#00ffff"align="center">ボタン</td>
                    <td style="background-color:#00ffff"align="center" width="400">購入日時</td>
                    <td style="background-color:#00ffff"align="center" width="200">配達方法</td>
                    <td style="background-color:#00ffff"align="center" width="200">購入金額</td>
                </tr>
                <c:forEach var="buyList" items="${buyList}">
                <tr>
                	<td><a class="btn btn-outline-primary" href="UserDataBuyDetail?id=${buyList.id}">詳細</a></td>
                    <td height="60" align="center" width="400">${buyList.buyDate}</td>
                    <td align="center" width="200">${buyList.deliveryname}</td>
                    <td align="center" width="200">${buyList.totalPrice}円</td>
                </tr>
                </c:forEach>
            </table>
                <p style="margin:20px;"></p>
                <div style="text-align:center;">
                    <a type="button" class="btn btn-outline-success" href="UserDataDetail">ユーザ情報詳細へ</a><br>
                    <p style="margin:40px;"></p>
                    <a href="Index" class="btn-flat-simple">
                        <i class="fa fa-caret-right"></i><u>TOPへ</u></a></p>
                </div>
	</body>
</html>
