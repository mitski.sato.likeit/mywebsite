<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>作品一覧（恋愛）</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　      <h1>マンガフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
                <div style="text-align:center;">
            <h3><font color="#ff4500">恋愛漫画</font></h3>
            <p style="margin:20px;"></p>
                <a class="btn btn-outline-primary" href="BookSports">スポーツ</a>
                <button type="button" class="btn btn-outline-danger" disabled>恋愛</button>
                <a class="btn btn-outline-warning" href="BookComedy">ギャグ</a>
                <a class="btn btn-outline-info" href="BookFantsy">SF・ファンタジー</a>
                <a class="btn btn-outline-dark" href="BookOther">その他</a>
            </div>
            <p style="margin:20px;"></p>
            <div style="text-align:center;">
                <a class="btn btn-light" href="BookList"><u>作品一覧に戻る</u></a>
            </div>
            <p style="margin:40px;"></p>
            <div style="text-align:center;">
                    <p>作品名をクリックすると詳細を表示します</p>
            </div>
                    <table  border="3" align="center" >
                    <tr>
                        <td bgcolor="#00FFFF" height="60" align="center">作品名</td>
                        <td bgcolor="#8FBC8F" align="center">ジャンル</td>
                        <td bgcolor="#FFA500" align="center">単価</td>
                    </tr>
                    <c:forEach var="bookList" items="${bookList}">
                    <tr>
                        <td align="center"><a class="btn btn-light" href="BookDetail?id=${bookList.id}"><u>${bookList.name}</u></a></td>
                        <td align="center">${bookList.janru}</td>
                        <td height="40" align="center">${bookList.price}円</td>
                    </tr>
                    </c:forEach>
        </table>
	</body>
</html>
