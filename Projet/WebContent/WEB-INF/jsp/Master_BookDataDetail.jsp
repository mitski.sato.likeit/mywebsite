<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>商品マスタ情報詳細</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
            <div class="alert alert-success" role="alert">
                <div style="text-align:center;">
    　　              <h1>マンガフェスタ</h1>
　　  　　      </div>
        </div>
</head>
	<body>
		  <div style="text-align:center;">
            <h2>作品情報詳細</h2>
        </div>

	<form action="" method="get">

    <p style="margin:20px;"></p>

      <table border="2" align="center" width="300">
        <tr>
            <td bgcolor="#00FFFF" align="center"height="50">作品名</td>
            <td align="center">${book.name}</td>
        </tr>
        <tr>
            <td bgcolor=#00FA9A align="center"height="50">ジャンル</td>
            <td align="center">${book.janru}</td>
        </tr>
        <tr>
            <td bgcolor="#FFA500" align="center"height="50">単価</td>
            <td align="center">${book.price}円</td>
        </tr>

        </table>
        <p style="margin:$0px;"></p>
        </form>
        <div style="text-align:center;">
		<a class="btn btn-outline-secondary"href="Master_BookList">戻る</a>
        </div>
	</body>
</html>
