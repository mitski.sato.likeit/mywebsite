<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>購入完了画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　      <h1>ブックフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>購入が完了しました</h2>
            <p style="margin:50px;"></p>
            <h4>購入詳細</h4>
        </div>
        <table border="2" align="center">
            <tr>
                <td style="background-color:#00ffff"align="center" width="400">購入日時</td>
                <td style="background-color:#00ffff"align="center" width="200">配達方法</td>
                <td style="background-color:#00ffff"align="center" width="200">購入金額</td>
            </tr>
            <tr >
                <td height="80" align="center" width="400">${resultBDB.buyDate}</td>
                <td align="center" width="200">${resultBDB.deliveryname}</td>
                <td align="center" width="200">${resultBDB.totalPrice}円</td>
            </tr>
        </table>
        <p style="margin:50px;"></p>
        <table border="2" align="center">
            <tr>
                <td style="background-color:#9acd32"align="center" width="500">商品名</td>
                 <td style="background-color:#9acd32"align="center" width="200">単価</td>
            </tr>
           	<c:forEach var="buyIDBList" items="${buyIDBList}">
            <tr>
                <td height="60" align="center" width="500">${buyIDBList.name}</td>
                <td align="center" width="200">${buyIDBList.price}円</td>
            </tr>
            </c:forEach>
        </table>
        <p style="margin:20px;"></p>
        <div style="text-align:center;">
            <a href="UserDataBuy" class="btn btn-outline-success">購入履歴の確認</a>
            <p style="margin:40px;"></p>
             <p style="margin-bottom:20px;">
                    <a href="Index" class="btn-flat-simple">
                        <i class="fa fa-caret-right"></i><u>TOPへ</u></a></p>
        </div>
	</body>
</html>