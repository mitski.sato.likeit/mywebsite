<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>購入確定画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　      <h1>マンガフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>購入確定画面</h2>
        </div>
                <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <table align="center" width="600">
                                <tr>
                                    <td height="20" bgcolor="#00FFFF" align="center">作品名</td>
                                    <td bgcolor=#ffd700	 align="center">価格</td>
                                </tr>
                                <c:forEach var="book" items="${cart}">
                                <tr>
                                    <td height="40" align="center">${book.name}</td>
                                    <td align="center">${book.price}円</td>
                                </tr>
                                </c:forEach>
                                <tr>
                                    <td height="40" align="center"></td>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td height="40" align="center">${ddb.name}</td>
                                    <td align="center">${ddb.price}円</td>
                                </tr>
                                <tr>
                                    <td height="60"bgcolor=#ff8c00 align="center">合計</td>
                                    <td bgcolor=#ff8c00 align="center">${BDB.totalPrice}円</td>
                                </tr>
                            </table>
                            <div style="text-align:center;">
                            <p style="margin:20px;"></p>
                            <p>以上の商品を購入します</p>
                            <p style="margin:10px;"></p>
                            <form action="BuyResult" method="post">
                            <button type="submit" class="btn btn-outline-danger">購入</button>
                            </form>
                            </div>
                        </div>
                    </div>
                <div style="text-align:center;">
                    <a href="BuySelect" class="btn btn-light"><u>購入選択画面に戻る</u></a>
                </div>

	</body>
</html>
