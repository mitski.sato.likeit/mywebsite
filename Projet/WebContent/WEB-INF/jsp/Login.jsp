<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
        <div class="alert alert-success" role="alert">
            <div style="text-align:center;">
                <h1>マンガフェスタ</h1>
            </div>
        </div>
    </div>
</head>
    <body>
        <div class="alert alert-primary" role="alert">
            <div style="text-align:center;">
                <div class="card-body">
                    <h2>ログイン画面</h1>
                </div>
               	<div style="text-align:center;">
    				<c:if test="${errMsg == null}" >
						 <span style="color:red;">${errMsg}</span>
					</c:if>
				</div>
                    <p style="margin-bottom:20px;">
                        <div class="col s6 offset-s3">
                            <form action="Login" method="post">
                                <p><input type ="text" placeholder="ログインID" name="loginId"><br>
                                <p style="margin-bottom:20px;"></p>
                                <input type ="text" placeholder="パスワード" name="password"><br>
                                <p style="margin-bottom:20px;"></p>
                                <input type = "submit" value ="ログイン">
                            </form>
                        </div>
                    <p style="margin-bottom:100px;"></p>
                    <p>初めてのご利用の方は<br>
                        こちらのボタンから新規登録をお願いします<br>
                    <p style="margin-bottom:20px;">
                    <a href="Regist" class="btn-flat-simple">
                        <i class="fa fa-caret-right"></i><u>新規登録</u></a></p>
            </div>
        </div>

    </body>
</html>
