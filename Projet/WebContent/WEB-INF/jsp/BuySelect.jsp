<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>購入選択</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　      <h1>マンガフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>購入選択</h2>
        </div>
        	<form action="Buy" method="get">
                <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <table align="center" width="600">
                                <tr>
                                    <td height="20" bgcolor="#00FFFF" align="center">作品名</td>
                                    <td bgcolor="#8FBC8F" align="center">ジャンル</td>
                                    <td bgcolor="#FFA500" align="center">価格</td>
                                </tr>
                                <c:forEach var="book" items="${cart}">
                                <tr>
                                    <td height="40" align="center">${book.name}</td>
                                    <td align="center">${book.janru}</td>
                                    <td align="center">${book.price}円</td>
                                </tr>
                                </c:forEach>
                            </table>
                            <p style="margin:30px;"></p>
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                    </div>
                                    <div class="input-field col s8 offset-s2 ">
                                        <select name="delivery_id">
                                        <c:forEach var="ddb" items="${ddbList}" >
                                            <option >${ddb.id}${ddb.name}</option>
                                            </c:forEach>
                                        </select> <label>配送方法</label>
                                    </div>
                                </div>
                            </div>
                            <p style="margin:30px;"></p>
                             <div style="text-align:center;">
                                 <button type=submit name=action class="btn btn-outline-danger">購入確定画面へ</button>
                             </div>
                          </div>
                    </div>
                </form>
                <p style="margin:10px;"></p>
                <div style="text-align:center;">
                    <a href="Cart" class="btn btn-light"><u>カートに戻る</u></a>
                </div>
	</body>
</html>
