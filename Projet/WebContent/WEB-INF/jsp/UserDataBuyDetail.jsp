<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>購入履歴詳細</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　     <h1>マンガフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>購入履歴詳細</h2>
            <table border="2" align="center">
                <tr>
                    <td style="background-color:#00ffff"align="center" width="400">購入日時</td>
                    <td style="background-color:#00ffff"align="center" width="200">配達方法</td>
                    <td style="background-color:#00ffff"align="center" width="200">購入金額</td>
                </tr>
                <tr>
                    <td  height="60" width="400">${buy.buyDate}</td>
                    <td  height="60" width="200">${buy.deliveryname}</td>
                    <td  height="60" width="200">${buy.totalPrice}円</td>
                </tr>
	            </table>
	            <p style="margin:50px;"></p>
	            <table border="2" align="center">
	                <tr>
	                    <td style="background-color:#9acd32"align="center" width="500">商品名</td>
	                    <td style="background-color:#9acd32"align="center" width="200">ジャンル</td>
	                    <td style="background-color:#9acd32"align="center" width="200">単価</td>
	                </tr>
	                <c:forEach var="itemList" items="${itemList}">
	                <tr>
	                    <td height="60" align="center" width="500">${itemList.name}</td>
	                    <td align="center" width="200">${itemList.janru}</td>
	                    <td align="center" width="200">${itemList.price}円</td>
	                </tr>
	                </c:forEach>
	            </table>
                    <div style="text-align:center;">
                    <p style="margin:40px;"></p>
                    <a href="UserDataBuy" class="btn-flat-simple">
                         <i class="fa fa-caret-right"></i><u>戻る</u></a>
               </div>
        </div>
    </body>
</html>