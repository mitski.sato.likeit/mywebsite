<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<div class="alert alert-success" role="alert">
	<div style="text-align: center;">
		<h1>マンガフェスタ</h1>
	</div>
</div>
</head>
<body>
	<div style="text-align: center;">
		<h2>作品詳細</h2>
	</div>

	<p style="margin: 20px;"></p>

	<table border="2" align="center" width="300">
		<tr>
			<td bgcolor="#00FFFF" align="center" height="40">作品名</td>
			<td align="center">${book.name}</td>
		</tr>
		<tr>
			<td bgcolor="#00FA9A" align="center" height="40">ジャンル</td>
			<td align="center">${book.janru}</td>
		</tr>
		<tr>
			<td bgcolor="#FFA500" align="center" height="40">単価</td>
			<td align="center">${book.price}円</td>
		</tr>
	</table>
	<p style="margin: $0px;"></p>
	<form action="ItemAdd" method="post">
	<input type="hidden" name="id" value="${book.id}">
	<div style="text-align: center;">
		<button type="submit" class="btn btn-danger">カートに入れる</button>
		<button type="button" onclick="history.back()" class="btn btn-outline-secondary">戻る</button>
	</div>
	</form>
		<div style="text-align: center;">
		<p>
		<p style="margin: 40px;"></p>
		<a class="btn btn-outline-primary" href="BookSports">スポーツ</a> <a
			class="btn btn-outline-danger" href="BookLove">恋愛</a> <a
			class="btn btn-outline-warning" href="BookComedy">ギャグ</a> <a
			class="btn btn-outline-info" href="BookFantsy">SF・ファンタジー</a> <a
			class="btn btn-outline-dark" href="BookOther">その他</a> <a
			class="btn btn-outline-secondary" href="BookList">ALL</a>
	</div>

</body>
</html>
