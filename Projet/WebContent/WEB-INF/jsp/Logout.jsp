<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログアウト画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
            <div style="text-align:center;">
                <div class="alert alert-success" role="alert">
                    <h1>マンガフェスタ</h1>
                </div>
            </div>
</head>
<body>
    <p style="margin-bottom:120px;"></p>
        <div style="text-align:center;">
            <h2>ログアウトしました。</h2>
            <p style="margin-bottom:50px;"></p>
            <a href="Login" class="navbar-link logout-link">戻る</a>
        </div>
	</body>
</html>
