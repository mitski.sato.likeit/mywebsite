<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報更新画面（ユーザ用）</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
            <h1>マンガフェスタ</h1>
        </div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>ユーザ情報更新</h2>
            	<div style="text-align:center;">
    				<c:if test="${errMsg != null}" >
						 <span style="color:red;">${errMsg}</span>
					</c:if>
				</div>
        </div>
        <form action="UserDataUpdate" method="post">
            <div style="text-align:center;">
                <p style="margin-bottom:20px;"></p>
                <p>ログインID&nbsp;&nbsp;&nbsp;${user.loginId}</p>
                <p><input type="hidden" value="${user.loginId}" name="loginId" ></p>
                <p style="margin-bottom:20px;"></p>
                <p><span class="line"><input type="text" placeholder="パスワード" name="password"></span></p>
                <p style="margin-bottom:20px;"></p>
                <p><span class="line"><input type="text" placeholder="パスワード確認" name="password2"></span></p>
                <p style="margin-bottom:20px;"></p>
                <p><span class="line"><input type="text" value="${user.name}" name="name"></span></p>
                <p>生年月日<br>
                <input type="date" value="${user.birthDate}"  name ="birthDate"></p>
                <p style="margin-bottom:40px;"></p>

                <button type="submit" class="btn btn-primary"id="${userInfo.id}">更新</button><br>
                <p style="margin-bottom:30;"></p>
                <a href="UserDataDetail?id=${userInfo.id}" class="btn btn-outline-secondary">中止</a>
            </div>
        </form>
	</body>
</html>
