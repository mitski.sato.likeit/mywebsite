<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細（ユーザ用）</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
            <div class="alert alert-success" role="alert">
                <div style="text-align:center;">
                    <h1>マンガフェスタ</h1>
                </div>
            </div>
</head>
	<body>
		 <div style="text-align:center;">
            <h2>ユーザ情報</h2>
        </div>
           <p style="margin:20px;"></p>
                <table align="center" width="300">
                    <tr>
                        <td height="40" align="center">ログインID</td>
                        <td align="center">${user.loginId}</td>
                    </tr>
                    <tr>
                        <td height="40" align="center">ユーザ名</td>
                        <td align="center">${user.name}</td>
                    </tr>
                    <tr>
                        <td height="40" align="center">生年月日</td>
                        <td align="center">${user.birthDate}</td>
                    </tr>
                    <tr>
                        <td align="center" height="40">登録日時</td>
                        <td align="center">${user.createDate}</td>
                    </tr>
                    <tr>
                        <td align="center" height="40">更新日時</td>
                        <td align="center">${user.upDate}</td>
                    </tr>
                </table>
            <p style="margin:$0px;"></p>
                <div style="text-align:center;">
                    <p>自分の情報を更新される方は更新ボタンを押してください。</p>
                    <a type="button"  class="btn btn-outline-primary"href="UserDataUpdate?id=${userInfo.id}">更新画面へ</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a type="button" class="btn btn-outline-success" href="UserDataBuy">購入履歴</a><br>
                    <p style="margin:20px;"></p>
                    <a href="Index" class="btn btn-outline-secondary">戻る</a>
                </div>
	</body>
</html>

