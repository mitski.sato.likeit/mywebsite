<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<div class="alert alert-success" role="alert">
	<div style="text-align: center;">
		<h1>マンガフェスタ</h1>
	</div>
	<div style="text-align: center;">
		<a href="Index" class="btn-flat-simple"><u>タイトルに戻る</u></a>
	</div>
</div>

</head>
<body>
	<div style="text-align: center;">
		<h2>作品一覧</h2>
	</div>
	<p style="margin: 20px;"></p>
	<div style="text-align: center;">
		<form action="BookList" method="post">
			<input type="submit" value="検索">&nbsp;&nbsp; <input
				type="text" placeholder="作品名で検索" name="name" size="40">
		</form>
	</div>
	<div style="text-align: center;">
		<p>
		<h4>ジャンルから選択</h4>
		<a class="btn btn-outline-primary" href="BookSports">スポーツ</a> <a
			class="btn btn-outline-danger" href="BookLove">恋愛</a> <a
			class="btn btn-outline-warning" href="BookComedy">ギャグ</a> <a
			class="btn btn-outline-info" href="BookFantsy">SF・ファンタジー</a> <a
			class="btn btn-outline-dark" href="BookOther">その他</a> <a
			class="btn btn-outline-secondary" href="BookList">ALL</a>
	</div>
	<div style="text-align: center;">
		<h4>作品リスト</h4>
	</div>
	<div style="text-align: center;">
		<p>作品名をクリックすると詳細を表示します</p>
	</div>
	<table border="3" align="center">
		<tr>
			<td bgcolor="#00FFFF" height="60" align="center">作品名</td>
			<td bgcolor="#8FBC8F" align="center">ジャンル</td>
			<td bgcolor="#FFA500" align="center">単価</td>
		</tr>
		<c:forEach var="bookList" items="${bookList}">
			<tr>
				<td align="center"><a class="btn btn-light" href="BookDetail?id=${bookList.id}"><u>${bookList.name}</u></a></td>
				<td align="center">${bookList.janru}</td>
				<td height="40" align="center">${bookList.price}円</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
