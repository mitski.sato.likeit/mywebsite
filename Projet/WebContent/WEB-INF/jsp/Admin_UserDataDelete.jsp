<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報削除画面（管理者用）</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
    　　      <h1>マンガフェスタ</h1>
　　  　　</div>
    </div>
</head>
	<body>
         <div style="text-align:center;">
            <h2>ユーザ情報削除</h2>
        </div>

	<form action="Admin_UserDataDelete" method="post">

        <p style="margin:20px;"></p>
    <div style="text-align:center;">
        <div class="alert alert-danger" role="alert">
            <h4>！こちらのユーザ情報を削除します！</h4>
        <table align="center" width="300">
            <tr>
                <td height="40" align="center">ログインID</td>
                <td align="center">${user.loginId}</td>
            </tr>
            <tr>
                <td height="40" align="center">ユーザ名</td>
                <td align="center">${user.name}</td>
            </tr>
            <tr>
                <td height="40" align="center">生年月日</td>
                <td align="center">${user.birthDate}</td>
            </tr>
            <tr>
                <td align="center" height="40">登録日時</td>
                <td align="center">${user.createDate}</td>
            </tr>
            <tr>
                <td align="center" height="40">更新日時</td>
                <td align="center">${user.upDate}</td>
            </tr>
        </table>
            <p style="margin:$0px;"></p>
            <input type="hidden" name ="loginId" value = "${user.loginId}">
            <button type="submit" class="btn btn-danger">削除</button><br>
        </div>
    </div>
 </form>
        <div style="text-align:center;">
            <a class="btn btn-outline-secondary" href="Admin_UserList">中止</a>
        </div>
	</body>
</html>
