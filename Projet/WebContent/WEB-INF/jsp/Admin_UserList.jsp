<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧（管理者用）</title>
           <link rel="stylesheet"
    	           href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	           integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	           crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
            <h1>マンガフェスタ</h1>
        </div>
</div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>ユーザ一覧</h2>
            <a href="Index" class="navbar-link logout-link"><u>タイトルに戻る</u></a>
            <p style="margin-bottom:20px;"></p>
        </div>
        <div style="padding: 10px; margin-bottom: 10px; border: 1px dotted #333333; background-color: #00FFFF; color: #000000;">
        	<form action="Admin_UserList" method="post">
            <h4>ユーザ検索</h4>
            <p style="margin-bottom:20px;"></p>
      	    <p style="text-align: center;">
            ログインID<input type="text" id="ログインID" name="login_id"></p>
			<p style="margin-bottom:20px;"></p>
            <p style="text-align: center;">
            &nbsp;ユーザ名&nbsp;&nbsp;<input type="text" name="name"></p>
            <p style="margin-bottom:20px;"></p>
            <p style="text-align: center;">
            生年月日<input type="date" name="date_start" id="date_start">
            ~ <input type="date" name="date_end" id="date_end"></p>
            <p align="right">
			<span><input type="submit" value="検索"></span>
			</form>
         </div>
            <p style="margin:20px;"></p>
                <div style="text-align:center;">
                    <h4>ユーザリスト</h4>
                </div>
                <p style="margin:20px;"></p>
                    <table  border="1" align="center" >
                        <tr>
                            <td height="60" align="center" bgcolor="#FFA500">ユーザ名</td>
                            <td align="center" bgcolor="#8FBC8F">ログインID</td>
                            <td align="center" bgcolor="#E6E6FA">詳細&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;更新&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;削除</td>
                        </tr>
                        <c:forEach var="userList" items="${userList}">
                        <tr>
                            <td align="center">${userList.name}</td>
                            <td align="center">${userList.loginId}</td>
                            <td height="40" align="center">
                            <a class="btn btn-primary" href="Admin_UserDataDetail?id=${userList.id}">詳細</a>
                            <a class="btn btn-warning" href="Admin_UserDataUpdate?id=${userList.id}">更新</a>
                            <a class="btn btn-danger"  href="Admin_UserDataDelete?id=${userList.id}">削除</a></td>
                        </tr>
                        </c:forEach>
                </table>
	</body>
</html>
