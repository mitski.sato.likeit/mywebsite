<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>商品マスタ情報更新画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
            <div class="alert alert-success" role="alert">
                <div style="text-align:center;">
    　　              <h1>マンガフェスタ</h1>
　　  　　      </div>
        </div>
</head>
	<body>
			<body>
        <div style="text-align:center;">
            <h2>作品情報更新</h2>
        </div>
            <div style="text-align:center;">
		    	<c:if test="${errMsg != null}" >
				 <span style="color:red;">${errMsg}</span>
				</c:if>
			</div>
		        <form action="Master_BookDataUpdate" method="post">
		            <div style="text-align:center;">
		                <p style="margin-bottom:50px;"></p>
		                <p>作品名<p>
		                <p style="margin-bottom:-10px;"></p>
		                <p><input type="text" value="${book.name}" name="name"></p>

		                <p>ジャンル</p>
		                <p style="margin-bottom:-10px;"></p>
		                <p><span class="line"><input type="text" value="${book.janru}" name="janru"></span></p>

		                <p>単価</p>
		                <p style="margin-bottom:-10px;"></p>
		                <p><span class="line"><input type="text" value="${book.price}" name="price"></span></p>

		                <input type="hidden" value="${book.id}" name="id">

		                <p style="margin-bottom:40px;"></p>
		                <button type="submit" class="btn btn-primary">更新</button><br>
		                <p style="margin-bottom:20px;"></p>
		                <a class="btn btn-outline-secondary" href="Master_BookList">中止</a>
		            </div>
		        </form>
	</body>
</html>
