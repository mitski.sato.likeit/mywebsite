<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>新規登録画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <div class="alert alert-success" role="alert">
        <div style="text-align:center;">
            <h1>マンガフェスタ</h1>
        </div>
    </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>新規登録</h2>
        </div>
        <div style="text-align:center;">
    		<c:if test="${errMsg != null}" >
				<span style="color:red;">${errMsg}</span>
			</c:if>
		</div>
            <form action="Regist" method="post">
                <div style="text-align:center;">
                    <p style="margin-bottom:20px;"></p>
                    <p><span class="line"><input type="text" placeholder="ログインID" name="loginId"></span></p>
                    <p><span class="line"><input type="text" placeholder="パスワード" name="password"></span></p>
                    <p><span class="line"><input type="text" placeholder="パスワード確認" name="password2"></span></p>
                    <p><span class="line"><input type="text" placeholder="ユーザ名" name="name"></span></p>
                    <p>生年月日<br>
                    <input type="date" value="${birthDate}"  name = birthDate></p>
                    <p style="margin-bottom:40px;"></p>
                    <p><button type="submit" class="btn btn-primary">登録</button></p>
                    <a href="Login" type="button" class="btn btn-outline-secondary">戻る</a>
                </div>
            </form>

	</body>
</html>
