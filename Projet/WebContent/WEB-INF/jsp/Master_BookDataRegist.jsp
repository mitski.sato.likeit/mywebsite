<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>商品マスタ新規登録画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
            <div class="alert alert-success" role="alert">
                <div style="text-align:center;">
    　　              <h1>マンガフェスタ</h1>
　　  　　      </div>
        </div>
</head>
	<body>
        <div style="text-align:center;">
            <h2>作品の新規登録</h2>
             <c:if test="${errMsg != null}" >
				<span style="color:red;">${errMsg}</span>
			</c:if>
        </div>
            <form action="Master_BookDataRegist" method="post">
                <div style="text-align:center;">
                    <p style="margin-bottom:20px;"></p>
                    <p><span class="line"><input type="text" placeholder="作品名" name="name"></span></p>
                    <p><span class="line"><input type="text" placeholder="ジャンル" name="janru"></span></p>
                    <p><span class="line"><input type="text" placeholder="金額" name="price"></span></p>
                    <p style="margin-bottom:40px;"></p>
                    <p><button type="submit" class="btn btn-primary">登録</button></p>
                    <a class="btn btn-outline-secondary" href="Master_BookList">戻る</a>
                </div>
            </form>
	</body>
</html>
