<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>商品マスタ情報削除画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
            <div class="alert alert-success" role="alert">
                <div style="text-align:center;">
    　　              <h1>マンガフェスタ</h1>
　　  　　      </div>
        </div>
</head>
	<body>
         <div style="text-align:center;">
            <h2>作品情報削除</h2>
        </div>

	<form action="Master_BookDataDelete" method="post">

        <p style="margin:20px;"></p>
    <div style="text-align:center;">
        <div class="alert alert-danger" role="alert">
            <h4>！こちらの作品を削除します！</h4>
        <table align="center" width="300">
            <tr>
                <td height="40" align="center">作品名</td>
                <td align="center">${book.name}</td>
            </tr>
            <tr>
                <td height="40" align="center">ジャンル</td>
                <td align="center">${book.janru}</td>
            </tr>
            <tr>
                <td align="center" height="40">単価</td>
                <td align="center">${book.price}</td>
            </tr>
        </table>
        	 <input type="hidden" value="${book.id}" name="id">
            <p style="margin:$0px;"></p>
            <button type="submit" class="btn btn-danger">削除</button>
        </div>
    </div>
 </form>
        <div style="text-align:center;">
            <a class="btn btn-outline-secondary" href="Master_BookList">中止</a>
        </div>
	</body>
</html>
