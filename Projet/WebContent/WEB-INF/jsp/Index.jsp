<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>タイトル画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<div class="alert alert-success" role="alert">
	<div style="text-align: center;">
		<h1>マンガフェスタ</h1>
	</div>
</div>
</head>
<body>
	<div style="text-align: center;">
		<h2>マンガフェスタへようこそ！</h2>
	</div>
	<p style="margin: 60px;"></p>
	<table align="center">
		<tr>
			<td><a class="btn btn-outline-primary" href="BookList">&nbsp;買い物をする&nbsp;</a></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>作品一覧ページに移行します<br> 商品を検索したり、ジャンルからお好みの商品を探すことができます
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
				<td><a class="btn btn-outline-success"
					href="UserDataDetail">&nbsp;&nbsp;&nbsp;ユーザ詳細&nbsp;&nbsp;&nbsp;</a></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>ユーザ情報の詳細画面に移行します<br> ユーザ情報の更新や購入履歴の確認ができます
				</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<c:if test="${userInfo.id == 1}">
				<td><a class="btn btn-outline-secondary" href="Master_BookList">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;商品一覧&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>商品の一覧を表示します。<br> 新規商品の登録、更新、削除、詳細の確認ができます。(管理者のみ)
				</td>
			</c:if>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<c:if test="${userInfo.id == 1}">
				<td><a class="btn btn-outline-secondary" href="Admin_UserList">&nbsp;&nbsp;&nbsp;ユーザ一覧&nbsp;&nbsp;&nbsp;</a></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>ユーザ一覧を表示します。<br> ユーザ情報の詳細の確認、更新、削除ができます。(管理者のみ)
				</td>
			</c:if>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><a href="Cart" class="btn btn-outline-warning">&nbsp;&nbsp;&nbsp;カート画面&nbsp;&nbsp;&nbsp;</a></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>カート画面を表示します。
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><a class="btn btn-outline-danger" href="Logout">&nbsp;&nbsp;&nbsp;ログアウト&nbsp;&nbsp;&nbsp;</a></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>ログアウトします</td>
		</tr>
	</table>

</body>
</html>
